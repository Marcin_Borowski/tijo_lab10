package pl.edu.pwsztar.service.impl;

import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.service.ChessService;

@Service
public class ChessServiceImpl implements ChessService {

    @Override
    public boolean moveCheckBishop(FigureMoveDto figureMoveDto) {
                int startcolumn = figureMoveDto.getStart().charAt(0);
                int destinationcolumn = figureMoveDto.getDestination().charAt(0);
                int startrow = figureMoveDto.getStart().charAt(2);
                int destinationrow = figureMoveDto.getDestination().charAt(2);

                if(startcolumn == destinationcolumn || startrow == destinationrow){
                    return false;
                    }

        return Math.abs(startcolumn - destinationcolumn) == Math.abs(startrow - destinationrow);
    }
}
